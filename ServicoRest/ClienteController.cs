﻿using System;
using System.Linq;

namespace ServicoRest
{
    public class ClienteController
    {
        public void Insert(ClienteEntity clienteEntity)
        {

        }

        public void UpdateSolicitacaoBackup(ClienteEntity clienteEntity)
        {
            using (db_ClientBackupsWSEntities context = new db_ClientBackupsWSEntities())
            {
                Cliente cliente = context.Cliente.Where(n => n.IDCliente == clienteEntity.IDCliente).First();

                cliente.UsuarioSolicitou = clienteEntity.UsuarioSolicitou;
                cliente.DateTimeSolicitacao = clienteEntity.DateTimeSolicitacao;
                cliente.BackupRequisitado = clienteEntity.BackupRequisitado;

                context.SaveChanges();

            }
        }


        public void UpdateBackupConcluido(ClienteEntity clienteEntity)
        {
            using (db_ClientBackupsWSEntities context = new db_ClientBackupsWSEntities())
            {
                Cliente cliente = context.Cliente.Where(n => n.IDCliente == clienteEntity.IDCliente).First();

                cliente.BackupRequisitado = false;
                cliente.DateTimeConclusao = clienteEntity.DateTimeConclusao;
                context.SaveChanges();
            }
        }

        public bool BackupRequisitado(int codCliente)
        {
            using (db_ClientBackupsWSEntities context = new db_ClientBackupsWSEntities())
            {
                Cliente cliente = context.Cliente.Where(n => n.IDCliente == codCliente).First();
                return cliente.BackupRequisitado;
            }
        }

        public string GetAutenticacaoSHA256(int codCliente)
        {
            using (db_ClientBackupsWSEntities context = new db_ClientBackupsWSEntities())
            {
                Cliente cliente = context.Cliente.Where(n => n.IDCliente == codCliente).FirstOrDefault();
                return cliente.SHA256Key;
            }
        }

        public ClienteEntity DadosCliente(int codCliente)
        {
            db_ClientBackupsWSEntities context = new db_ClientBackupsWSEntities();

            ClienteEntity clienteEntity = new ClienteEntity();


            Cliente cliente = (from n in context.Cliente where n.IDCliente == codCliente select n).First();


            clienteEntity.IDCliente = cliente.IDCliente;
            clienteEntity.BackupRequisitado = cliente.BackupRequisitado;
            clienteEntity.UsuarioSolicitou = cliente.UsuarioSolicitou;
            clienteEntity.DateTimeSolicitacao = DateTime.Now; //'(DateTime)cli.DateTimeSolicitacao;
            clienteEntity.DateTimeConclusao = DateTime.Now; //(DateTime)cli.DateTimeConclusao;
            clienteEntity.SHA256Key = cliente.SHA256Key;



            return clienteEntity;
        }


    }
}