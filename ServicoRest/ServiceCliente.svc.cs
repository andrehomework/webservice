﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace ServicoRest
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da classe "ServiceCliente" no arquivo de código, svc e configuração ao mesmo tempo.
    // OBSERVAÇÃO: Para iniciar o cliente de teste do WCF para testar esse serviço, selecione ServiceCliente.svc ou ServiceCliente.svc.cs no Gerenciador de Soluções e inicie a depuração.
    public class ServiceCliente : IServiceCliente
    {
        ClienteController clienteController = new ClienteController();

        public string UpdateSolicitacaoBackup(ClienteEntity clienteEntity)
        {

            return "Entrei aqui de boa";
        }

        public String UpdateBackupConcluido(ClienteEntity clienteEntity)
        {

            return "Entrei aqui de boa";
        }

        public bool BackupRequisitado(string codigo)
        {
            
                return clienteController.BackupRequisitado(Convert.ToInt32(codigo));

        }

        public string AutenticacaoSHA(string codCliente)
        {
            return clienteController.GetAutenticacaoSHA256(Convert.ToInt32(codCliente));
        }

        public ClienteEntity DadosCliente(string codCliente)
        {
            return clienteController.DadosCliente(Convert.ToInt32(codCliente));
            
            //return clienteController.DadosCliente(Convert.ToInt32(codCliente));
        }


    }
}
