﻿using System;
using System.Runtime.Serialization;

namespace ServicoRest
{
    [DataContract]
    public class ClienteEntity
    {
        private int id;
        private int idCliente;
        private bool backupRequisitado;
        private string usuarioSolicitou;
        private DateTime dateTimeSolcitacao;
        private DateTime dateTimeConclusao;
        private string sha256Key;


        [DataMember]
        public int ID
        {
            get
            {
                return id;
            }
        }

        [DataMember]
        public int IDCliente
        {
            get
            {
                return idCliente;
            }
            set
            {
                idCliente = value;
            }

        }

        [DataMember]
        public bool BackupRequisitado
        {
            get
            {
                return backupRequisitado;
            }
            set
            {
                backupRequisitado = value;
            }

        }

        [DataMember]
        public string UsuarioSolicitou
        {
            get
            {
                return usuarioSolicitou;
            }
            set
            {
                usuarioSolicitou = value;
            }
        }

        [DataMember]
        public DateTime DateTimeSolicitacao
        {
            get
            {
                return dateTimeSolcitacao;
            }
            set
            {
                dateTimeSolcitacao = value;
            }
        }

        [DataMember]
        public DateTime DateTimeConclusao
        {
            get
            {
                return dateTimeConclusao;
            }
            set
            {
                dateTimeConclusao = value;
            }
        }

        [DataMember]
        public string SHA256Key
        {
            get
            {
                return sha256Key;
            }
            set
            {
                sha256Key = value;
            }
        }
    }
}